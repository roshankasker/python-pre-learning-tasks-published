def factors(number):

    # ==============
    return [x for x in range(2, number//2+1) if number%x == 0]
    # ==============

print(factors(15)) # Should print [3, 5] to the console
print(factors(12)) # Should print [2, 3, 4, 6] to the console
print(factors(13)) # Should print “[]” (an empty list) to the console
